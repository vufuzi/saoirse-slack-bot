'use strict';

const https = require('https');

const RtmClient = require('@slack/client').RtmClient;
const WebClient = require('@slack/client').WebClient;
const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
const RTM_EVENTS = require('@slack/client').RTM_EVENTS;

const rtm = new RtmClient(SlackAPIToken, {
  // logLevel: 'debug'
});
const wc = new WebClient(SlackAPIToken);
const spotifyRegex = /spotify:(track):([a-zA-Z0-9]{22})/g;
const spotifyURLRegex = /(http|https)\:\/\/open\.spotify\.com\/(track)\/([a-zA-Z0-9]+)/g;
const tidalURLRegex = /(https?\:\/\/)*(.*)tidal\.com\/(track)\/([a-zA-Z0-9]+)/g;

function getImageForItem (item, callback) {
  let prot = https;

  let opt = {
    host: 'api.spotify.com',
    path: `/v1/tracks/${item.spotify_id}`
  };

  let req = prot.request(opt, function (res) {
    let output = '';

    res.setEncoding('utf8');

    res.on('data', function (chunk) {
      output += chunk;
    });

    res.on('end', function () {
      let obj = JSON.parse(output);
      let images = obj.album.images;
      let image = images[images.length - 1].url;

      callback(res.statusCode, image);
    });
  });

  req.on('error', function (err) {
    console.log(err.message);
  });

  req.end();
}

function fetchConvertedItem (item, callback) {
  let prot = https;

  let opt = {
    host: 'saoirse.vufuzi.se',
    path: `/track/${item.service}/${item.id}`
  };

  let req = prot.request(opt, function (res) {
    let output = '';

    res.setEncoding('utf8');

    res.on('data', function (chunk) {
      output += chunk;
    });

    res.on('end', function () {
      let obj = JSON.parse(output);
      callback(res.statusCode, obj);
    });
  });

  req.on('error', function (err) {
    console.log(err.message);
  });

  req.end();
}

function parseMessage (text) {
  if (!text) return;

  let spotifyLinkMatch = text.match(spotifyRegex);
  let spotifyOpenLinkMatch = text.match(spotifyURLRegex);
  let tidalLinkMatch = text.match(tidalURLRegex);
  let results = [];

  // spotify:track:0OMvPuFZbl94WC3YUrj3ME
  if (spotifyLinkMatch) {
    for (let match of spotifyLinkMatch) {
      let type = match.split(':')[1];
      let id = match.split(':')[2];
      let service = 'spotify';

      results.push({
        type,
        id,
        service
      });
    }
  }

  // Matches http://open.spotify.com/track/0OMvPuFZbl94WC3YUrj3ME
  if (spotifyOpenLinkMatch) {
    for (let m of spotifyOpenLinkMatch) {
      let match = m.split('open.')[1];
      let type = match.split('/')[1];
      let id = match.split('/')[2];
      let service = 'spotify';

      results.push({
        type,
        id,
        service
      });
    }
  }

  // tidal.com/album/57946354
  if (tidalLinkMatch) {
    for (let m of tidalLinkMatch) {
      let match = m.split('|')[1].split('.com')[1];
      let type = match.split('/')[1];
      let id = match.split('/')[2];
      let service = 'tidal';

      console.log({
        type,
        id,
        service
      });

      results.push({
        type,
        id,
        service
      });
    }
  }

  return results;
}

function convertItems (items, channelId) {
  for (let item of items) {
    fetchConvertedItem(item, function (code, result) {
      var json = JSON.stringify(result);

      console.log(result);

      if (code === 200 && json) {
        breadcastMediaItem(json, channelId);
      } else {
        console.log(code, result);
      }
    });
  }
}

function breadcastMediaItem (json, channelId) {
  let item = JSON.parse(json);

  if (!item) return;

  getImageForItem(item, function (status, imageLink) {
    let spotifyLink = `https://open.spotify.com/track/${item.spotify_id}`;
    let tidalLink = `https://listen.tidal.com/track/${item.tidal_id}`;

    let data = {
      username: `${item.artist}´s Secret Helper`,
      icon_url: imageLink,
      attachments: []
    };

    data.attachments.push({
      title: `${item.name}`,
      text: `by ${item.artist}`,
      thumb_url: imageLink
    });

    data.attachments.push({
      fallback: `Play ${item.name} by ${item.artist} on TIDAL - ${tidalLink}`,
      title: `Open in TIDAL`,
      title_link: tidalLink,
      color: '#00FEFE'
    });

    data.attachments.push({
      fallback: `Play ${item.name} by ${item.artist} on Spotify - ${spotifyLink}`,
      title: `Open in Spotify`,
      title_link: spotifyLink,
      color: '#2FD566'
    });

    /*data.attachments.push({
      fallback: `The ID for this song on Apple Music and iTunes is ${item.itunes_id}`,
      title: `Apple Music and iTunes...`,
      text: `...does not have a way to build URLs to their tracks based on an ID. Here's the ID though: ${item.itunes_id}`,
      color: '#000'
    });*/

    wc.chat.postMessage(channelId, `Listen to ${item.name} by ${item.artist}!`, data, function (a) {
      console.log(a, 'message sent I hope');
    });
  });
}

rtm.on(RTM_EVENTS.MESSAGE, function (message) {
  if (!message.text) return;

  let items = parseMessage(message.text);

  console.log(message);

  if (items) {
    convertItems(items, message.channel);
  }
});

rtm.start();
